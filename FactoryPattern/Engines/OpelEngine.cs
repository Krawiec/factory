﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Engines
{
    public class OpelEngine : IEngine
    {
        public string Title()
        {
            return "OpelEngine";
        }
    }
}
