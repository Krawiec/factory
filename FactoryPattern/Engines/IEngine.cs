﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Engines
{
    public interface IEngine
    {
        string Title();
    }
}
