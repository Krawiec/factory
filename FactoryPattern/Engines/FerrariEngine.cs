﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Engines
{
    public class FerrariEngine : IEngine
    {
        public string Title()
        {
            return "FerrariEngine";
        }
    }
}
