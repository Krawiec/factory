﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoryPattern.Engines;
using FactoryPattern.Pistons;

namespace FactoryPattern.CarFactories
{
    public class FerrariFactory : ICarFactory
    {
        public bool IsAplicable(CarType carType)
        {
            return carType == CarType.Ferrari;
        }
        public IEngine CreateEngine()
        {
            return new FerrariEngine();
        }

        public IPistons CreatePistons()
        {
            return new FerrariPistons();
        }
    }
}
