﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.CarFactories
{
    public enum CarType
    {
        Ferrari = 1,
        Opel = 2
    }
}
