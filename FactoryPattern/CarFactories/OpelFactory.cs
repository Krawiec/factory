﻿using FactoryPattern.Engines;
using FactoryPattern.Pistons;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.CarFactories
{
    public class OpelFactory : ICarFactory
    {
        public bool IsAplicable(CarType carType)
        {
            return carType == CarType.Opel;
        }

        public IEngine CreateEngine()
        {
            return new OpelEngine();
        }

        public IPistons CreatePistons()
        {
            return new OpelPistons();
        }
    }
}
