﻿using FactoryPattern.Engines;
using FactoryPattern.Pistons;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.CarFactories
{
    public interface ICarFactory
    {
        bool IsAplicable(CarType carType);
        IEngine CreateEngine();
        IPistons CreatePistons();
    }
}
