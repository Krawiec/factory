﻿using FactoryPattern.CarFactories;
using System;

namespace FactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new Client();
            client.CreateCar(CarType.Ferrari);
            client.PrintCar();
            Console.ReadLine();
        }
    }
}
