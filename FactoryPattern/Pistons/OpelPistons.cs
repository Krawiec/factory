﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Pistons
{
    public class OpelPistons : IPistons
    {
        public string Title()
        {
            return "OpelPistons";
        }
    }
}
