﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Pistons
{
    public interface IPistons
    {
        string Title();
    }
}
