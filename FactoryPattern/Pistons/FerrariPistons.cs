﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Pistons
{
    public class FerrariPistons : IPistons
    {
        public string Title()
        {
            return "FerrariPistons";
        }
    }
}
