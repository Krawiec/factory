﻿using FactoryPattern.CarFactories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FactoryPattern.Engines;
using FactoryPattern.Pistons;

namespace FactoryPattern
{
    public class Client
    {
        private IEngine _engine;
        private IPistons _pistons;

        /// <summary>
        /// Should be prvided by IoC
        /// </summary>
        private IEnumerable<ICarFactory> _factories;

        public Client()
        {
            _factories  = new List<ICarFactory>()
            {
                new OpelFactory(),
                new FerrariFactory()
            };
        }
        public void CreateCar(CarType carType)
        {
            var factory = _factories.FirstOrDefault(x => x.IsAplicable(carType));
            this._engine = factory.CreateEngine();
            this._pistons = factory.CreatePistons();
        }

        public void PrintCar()
        {
            Console.WriteLine(
                "I have engine {0}! I have a pistons {1}.", _engine.Title(), _pistons.Title()
                );
        }
    }
}
